<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'namespace' => 'API\V1', // it is added in controller function name
    'prefix' => 'v1', //it is added in url
//    'as' => '.' //route name
], function () {

    Route::post('login','AuthController@login')->name('login');
    Route::post('register','AuthController@register')->name('register');

    Route::group(['middleware' => 'jwt-auth'], function(){

        Route::get('logout','AuthController@logout')->name('logout');

        // Route list for company
        Route::get('get-companies', 'CompanyController@getList');
        Route::post('create-company', 'CompanyController@store');


        // Route list for Contact
        Route::get('get-contacts/{page?}', 'ContactController@getContactList');
        Route::post('create-contact', 'ContactController@store');
        Route::post('edit-contact/{id}', 'ContactController@update');
        Route::post('search-contact', 'ContactController@searchContact');
        Route::post('add-contact-note', 'ContactController@addContactNote');
        Route::get('contact-by-company/{company}', 'ContactController@contactByCompany');

        Route::get('contact/{id}', 'ContactController@getContactById');
    });

});
