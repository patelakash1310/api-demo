<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\ResponseController;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\User;
use Validator;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;


class AuthController extends ResponseController
{

    /**
     * Generate token for authentication
     *
     * @return \Illuminate\Http\Response
     */

    protected function respondWithToken($token)
    {
        return response()->json([
            'Authorization' => 'Bearer ' . $token,
//            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

       try{

           $validator = Validator::make($request->all(), [
               'email' => 'required|email',
               'password' => 'required',
           ]);
           if ($validator->fails()) {
               return response()->json(['success' => false,'message' => $validator->messages()->first(),'data'=>[] ], 200);
           }

           $credentials = request(['email', 'password']);

           if (!$token = Auth::guard('api')->attempt($credentials)) {
               return $this->ApiResponseError([], 'Invalid Email and Password', 401);

           }else {

               $user = Auth::guard('api')->user();


               $data['user']=$user;
               $data['token']= $this->respondWithToken($token)->original;;

               return $this->ApiResponseSuccess($data, 'Logged in Successfully', 200);
           }
       }catch (\Exception $e){

           return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
       }
    }

    public function register(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false,'message' => $validator->messages()->first(),'data'=>[] ], 200);
            }

            $user= User::create([
                   'name'=>$request->name,
                   'email'=>$request->email,
                   'password'=>bcrypt($request->password),
                ]);


            return $this->ApiResponseSuccess($user, 'User Registered Successfully', 200);
        }catch (\Exception $e)
        {
            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }

    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        try{
            $Token = JWTAuth::parseToken()->authenticate();
            $user=User::find($Token->id);
            return $this->ApiResponseSuccess($user, 'User Detail', 200);
        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        try{
            $Token = JWTAuth::parseToken()->authenticate();
            $user=User::find($Token->id);

            Auth::guard('api')->logout();
            return $this->ApiResponseSuccess([], 'Logout Successfully.', 200);
        }catch (\Exception $e){
            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }



    public static function quickRandom($length = 60)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

}
