<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\ResponseController;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use JWTAuth;


class CompanyController extends ResponseController
{

    /**
     * store the specified resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false,'message' => $validator->messages()->first(),'data'=>[] ], 200);
            }

            $Token = JWTAuth::parseToken()->authenticate();
            $user=User::find($Token->id);


            Company::create([
                'name'=>$request->name,
            ]);

            return $this->ApiResponseSuccess([], 'Company Created Successfully.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * Return list of company.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        try{

            $Token = JWTAuth::parseToken()->authenticate();
            $user=User::find($Token->id);


          $companies =Company::get();
            return $this->ApiResponseSuccess($companies, 'Company list.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }
}
