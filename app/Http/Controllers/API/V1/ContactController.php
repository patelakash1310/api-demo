<?php

namespace App\Http\Controllers\API\V1;

use App\ContactNotes;
use App\Http\Controllers\API\ResponseController;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Contact;
use App\User;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;

class ContactController extends ResponseController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'country_code' => 'required',
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false,'message' => $validator->messages()->first(),'data'=>[] ], 200);
            }

            $Token = JWTAuth::parseToken()->authenticate();
            $user=User::find($Token->id);

            Contact::create([
                'name'=>$request->name,
                'country_code'=>$request->country_code,
                'phone'=>$request->phone,
                'company_id'=>$request->company_id,
            ]);

            return $this->ApiResponseSuccess([], 'Contact Created Successfully.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'country_code' => 'required',
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false,'message' => $validator->messages()->first(),'data'=>[] ], 200);
            }

            $contact = Contact::with('notes')->find($id);

            $contact->name = ($request->name) ? $request->name : $contact->name;
            $contact->country_code = ($request->country_code) ? $request->country_code : $contact->country_code;
            $contact->phone = ($request->phone) ? $request->phone : $contact->phone;

            $contact->save();


            return $this->ApiResponseSuccess([], 'Contact Updated Successfully.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }


    /**
     * Return a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContactList($page = 1)
    {
        try{

            $contacts =Contact::with('notes')->simplePaginate(10);
//            $contacts =Contact::get();

            return $this->ApiResponseSuccess($contacts, 'List of Contacts.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }


    /**
     * Return the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getContactById($id)
    {
        try{

            $contact =Contact::with('company','notes')->find($id);

            return $this->ApiResponseSuccess($contact, 'get contact by Id.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * Return the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchContact(Request $request)
    {
        try{

            $input =  $request->search_string;
            $contact =Contact::with('notes')->where('name','LIKE','%' . $input . '%')->orWhereHas('company', function($q) use ($input){
                return $q->where('name', 'LIKE', '%' . $input . '%');
            })->get();

            return $this->ApiResponseSuccess($contact, 'Search contact result.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * store notes for the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addContactNote(Request $request)
    {
        try{

            $note =  $request->note;
            $contactId =  $request->contact_id;

            ContactNotes::create([
                'contact_id'=>$contactId,
                'note'=>$note
            ]);

            return $this->ApiResponseSuccess([], 'contact notes added successfully.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }

    /**
     * Return the specified resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function contactByCompany($company)
    {
        try{

         $contact = Contact::where('company_id',$company)->get();

         return $this->ApiResponseSuccess($contact, 'contact by company.', 200);

        }catch (\Exception $e){

            return $this->ApiResponseError([], $e->getMessage().'-->'.$e->getLine(), 401);
        }
    }
}
