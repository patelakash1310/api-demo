<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function ApiResponseError($data, $message, $code,$error=array()) {
        $responseArr = ['response_code' => $code, 'success' => false, 'message' => $message,'data' => $data];
        return response($responseArr, $code);
    }

    public function ApiResponseSuccess($data, $message, $code) {
        $responseArr = ['response_code' => $code, 'success'=>true, 'message'=>$message, 'data' => $data ];
        return response($responseArr, $code);
    }
}
