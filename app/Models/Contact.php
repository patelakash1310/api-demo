<?php

namespace App\Models;

use App\ContactNotes;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['company_id','name','country_code','phone','notes'];

    public function company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }

    public function notes()
    {
        return $this->hasMany(ContactNotes::class,'contact_id','id');
    }
}
