<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactNotes extends Model
{
    protected $fillable = ['contact_id','note'];
}
