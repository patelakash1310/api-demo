
## Laravel Contact Manager API application

## Task List
1. Create and edit contacts
2. Retrieve a paginated list of all contacts
3. Search for contacts by name or company
4. Retrieve a single contact.
5. Can store multiple contacts for the same company.
6. Can store notes against a contact.
7. List all contacts at a given company.
8. List all companies.
 
 
 
## Steps for Installation


- Clone repository using git clone https://gitlab.com/patelakash1310/api-demo.git
- Rename .env.example file to .env your project root and fill the database information. (windows wont let you do it, so you have to open your console cd your project root directory and run mv .env.example .env )
- Open the console and cd your project root directory
- Run composer install or php composer.phar install
- Run php artisan key:generate
- Run php artisan migrate
- Run php artisan db:seed to run seeders
- Run php artisan serve



## Default Credentials

Email : akash@demo.com
Password : 123456

## Postman Collection Detail

https://www.getpostman.com/collections/e0f76da002e027c60cdd

Note: Token generated in Login API after You need to pass Authorization in header.