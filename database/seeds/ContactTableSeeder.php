<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Contact::truncate();
        $faker = Faker\Factory::create();

        for ($i=1; $i<=15; $i++)
        {
            \App\Models\Contact::create([
                'name'=>$faker->name,
                'country_code'=>'44',
                'phone'=>$faker->phoneNumber             ,
                'company_id'=>rand(1,5),
            ]);
        }
    }
}
