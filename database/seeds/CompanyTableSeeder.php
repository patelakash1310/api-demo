<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Company::truncate();
        $faker = Faker\Factory::create();

        for ($i=1; $i<=5; $i++)
        {
            \App\Models\Company::create([
               'name'=>$faker->company,
            ]);
        }
    }
}
